import React, {Component} from 'react';
import {view} from "@risingstack/react-easy-state";
import SignedInHeader from "../Components/Header/SignedInHeader";
import CreateAPostModal from "../Components/Modals/CreateAPostModal";

class UserSignedInTemplate extends Component{

    render(){
        return(
            <div style={{background: '#EEE', minHeight: '100vh'}}>
                <SignedInHeader />
                <div className='container'>
                    {this.props.children}
                </div>
                <CreateAPostModal />
            </div>
        )
    }

}

export default view(UserSignedInTemplate);