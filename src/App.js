import React from 'react';
import {view} from "@risingstack/react-easy-state";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

import './App.css'

import GlobalState from "./State/GlobalState";
import UserSignedInTemplate from "./Templates/UserSignedInTemplate";
import SiteHome from "./Pages/SiteHome";
import UserHome from "./Pages/UserHome";
import SignUp from "./Pages/SignUp";
import SignIn from "./Pages/SignIn";

GlobalState.init();


function renderAppLoading(){
    return (
        <div>
            Loading
        </div>
    );
}

function renderAppSignedIn(){
    return(
        <Router>
            <UserSignedInTemplate>
                <Switch>
                    <Route path='/' exact component={UserHome} />
                </Switch>
            </UserSignedInTemplate>
        </Router>
    );
}

function renderAppNoSignIn(){
    return(
        <Router>
            <Switch>
                <Route path='/' exact component={SiteHome} />
                <Route path='/sign-up' component={SignUp} />
                <Route path='/sign-in' component={SignIn} />
            </Switch>
        </Router>
    );
}

function App() {
  return (
      <div className='App'>
          {GlobalState.isLoading && renderAppLoading()}
          {!GlobalState.isLoading && GlobalState.isLoggedIn && renderAppSignedIn()}
          {!GlobalState.isLoading && !GlobalState.isLoggedIn && renderAppNoSignIn()}
      </div>
  );
}

export default view(App);