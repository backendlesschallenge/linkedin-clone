import React, {Component} from 'react';
import {view} from "@risingstack/react-easy-state";
import './HomeSuggestedSearches.css'


class HomeSuggestedSearches extends Component{

    state = {
        showHidden: false
    }

    constructor() {
        super();

        this.shownSearches = [
            'Engineering', 'Business Development', 'Finance', 'Administrative Assistant', 'Retail Associate',
            'Customer Service', 'Operations', 'Information Technology', 'Marketing', 'Human Resources'
        ];
        this.hiddenSearches = [
            'Healthcare Service', 'Sales', 'Program and Project Management', 'Accounting', 'Arts and Design',
            'Community and Social Services', 'Consulting', 'Education', 'Entrepreneurship', 'Legal',
            'Media and Communications', 'Military and Protective Services', 'Product Management',
            'Purchasing', 'Quality Assurance', 'Real Estate', 'Research', 'Support', 'Administrative'
        ]
    }



    getSearches() {
        const searches = [];

        this.shownSearches.forEach(search => {
            searches.push(search);
        });

        if(this.state.showHidden){
            this.hiddenSearches.forEach(search => {
                searches.push(search);
            });
        }

        return searches;
    }

    render(){
        return (
            <div className='home-suggested-searches'>
                <div className='container'>
                    <div className='row'>
                        <div className='col-4'>
                            <h2 className='home-suggested-searches-title'>Find open jobs and internships</h2>
                        </div>
                        <div className='col-1' />
                        <div className='col-7'>
                            <div>
                                SUGGESTED SEARCHES
                            </div>

                            {this.getSearches().map((search, index) => (
                                <button key={index} className='btn btn-lg btn-outline-dark home-suggested-searches-button'>{search}</button>
                            ))}

                            <div>
                                <a onClick={() => this.setState({
                                    showHidden: !this.state.showHidden
                                })}>
                                    {this.state.showHidden ? 'Show Less' : 'Show More'}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default view(HomeSuggestedSearches);