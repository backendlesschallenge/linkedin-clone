import React from "react";
import {view} from "@risingstack/react-easy-state";
import './HomePostJob.css'

function HomePostJob(){
    return(
        <div className='home-post-job-section'>
            <div className='container'>
                <div className='row'>
                    <div className='col-5'>
                        <h2 className='home-post-job-section-title'>Post your job and find the people you need</h2>
                    </div>
                    <div className='col-1' />
                    <div className='col-6'>
                        <button className='btn btn-lg btn-outline-info home-post-job-button'>Post a job</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default view(HomePostJob);