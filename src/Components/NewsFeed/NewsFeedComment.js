import React, {Component} from 'react';
import {view} from "@risingstack/react-easy-state";
import Gravatar from 'react-gravatar';
import TimeAgo from '../../Libs/TimeAgo'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faThumbsUp} from "@fortawesome/free-solid-svg-icons/faThumbsUp";
import {faThumbsDown} from "@fortawesome/free-solid-svg-icons/faThumbsDown";
import {faGrinSquintTears} from "@fortawesome/free-solid-svg-icons/faGrinSquintTears";
import {faGlassCheers} from "@fortawesome/free-solid-svg-icons/faGlassCheers";
import {faHeart} from "@fortawesome/free-solid-svg-icons/faHeart";
import {OverlayTrigger} from "react-bootstrap";


import './NewsFeedComment.css'
import NewsFeedState from "../../State/NewsFeedState";
import GlobalState from "../../State/GlobalState";
import {Link} from "react-router-dom";


class NewsFeedComment extends Component{

    state = {
        mouseInReactionControl: false,
        reactionControlShow: false
    };

    getCurrentReaction() {
        const {reactions} = this.props.data;
        let currentReaction = null;

        reactions.forEach(reaction => {
            if(reaction.user === GlobalState.user.objectId){
                currentReaction = reaction.reaction;
            }
        });

        return currentReaction;
    }

    renderReactionCounts() {
        let likes = 0,
            dislikes = 0,
            laughs = 0,
            loves = 0,
            cheers = 0;

        const {reactions} = this.props.data;

        reactions.forEach(reaction => {
            switch (reaction.reaction.toLowerCase()){
                case 'like':
                    likes++;
                    break;
                case 'dislike':
                    dislikes++;
                    break;
                case 'laugh':
                    laughs++;
                    break;
                case 'love':
                    loves++;
                    break;
                case 'cheers':
                    cheers++;
                    break;
            }
        });

        if(likes || dislikes || laughs || loves || cheers){
            const totalCount = likes + dislikes + laughs + loves + cheers;
            return (
                <div className='news-feed-post-reaction-count-container'>
                    {likes !== 0 && <FontAwesomeIcon icon={faThumbsUp} className='news-feed-post-icon-green' />}
                    {dislikes !== 0 && <FontAwesomeIcon icon={faThumbsDown} className='news-feed-post-icon-red' />}
                    {laughs !== 0 && <FontAwesomeIcon icon={faGrinSquintTears} className='news-feed-post-icon-yellow' />}
                    {loves !== 0 && <FontAwesomeIcon icon={faHeart} className='news-feed-post-icon-pink' />}
                    {cheers !== 0 && <FontAwesomeIcon icon={faGlassCheers} className='news-feed-post-icon-gray' />}
                    <span className='news-feed-post-reaction-count ml-2'>
                        {totalCount}
                    </span>
                </div>
            );
        } else {
            return null;
        }
    }

    renderReactionControlElement(icon, onClick, iconClass) {
        return (
            <button className={`btn btn-light mr-1 news-feed-post-icon-${iconClass}`} onClick={() => {
                onClick();
                this.setState({
                    mouseInReactionControl: false,
                    reactionControlShow: false
                });
            }}><FontAwesomeIcon icon={icon} /></button>
        );
    }

    renderReactionControl(){
        const {index, postIndex} = this.props;
        return (
            <div
                className='news-feed-post-actions-reaction-container'
                onMouseEnter={() => this.setState({mouseInReactionControl: true})}
                onMouseLeave={() => this.setState({mouseInReactionControl: false})}
            >
                {this.renderReactionControlElement(faThumbsUp, () => NewsFeedState.addCommentReaction(postIndex, index, 'like'), 'green')}
                {this.renderReactionControlElement(faThumbsDown, () => NewsFeedState.addCommentReaction(postIndex, index, 'dislike'), 'red')}
                {this.renderReactionControlElement(faGrinSquintTears, () => NewsFeedState.addCommentReaction(postIndex, index, 'laugh'), 'yellow')}
                {this.renderReactionControlElement(faHeart, () => NewsFeedState.addCommentReaction(postIndex, index, 'love'), 'pink')}
                {this.renderReactionControlElement(faGlassCheers, () => NewsFeedState.addCommentReaction(postIndex, index, 'cheers'), 'gray')}
            </div>);
    }

    renderReactButton() {
        const {index, postIndex} = this.props;

        const currentReaction = this.getCurrentReaction();
        if(currentReaction){
            switch (currentReaction.toLowerCase()){
                case 'like':
                    return (<button className='btn btn-sm news-feed-comment-react-button news-feed-post-icon-green' onClick={() => NewsFeedState.removeCommentReaction(postIndex, index)}><FontAwesomeIcon icon={faThumbsUp} /> Liked</button>);
                case 'dislike':
                    return (<button className='btn btn-sm news-feed-comment-react-button news-feed-post-icon-red' onClick={() => NewsFeedState.removeCommentReaction(postIndex, index)}><FontAwesomeIcon icon={faThumbsDown} /> Disliked</button>);
                case 'laugh':
                    return (<button className='btn btn-sm news-feed-comment-react-button news-feed-post-icon-yellow' onClick={() => NewsFeedState.removeCommentReaction(postIndex, index)}><FontAwesomeIcon icon={faGrinSquintTears} /> Laughed</button>);
                case 'love':
                    return (<button className='btn btn-sm news-feed-comment-react-button news-feed-post-icon-pink' onClick={() => NewsFeedState.removeCommentReaction(postIndex, index)}><FontAwesomeIcon icon={faHeart} /> Loved</button>);
                case 'cheers':
                    return (<button className='btn btn-sm news-feed-comment-react-button news-feed-post-icon-gray' onClick={() => NewsFeedState.removeCommentReaction(postIndex, index)}><FontAwesomeIcon icon={faGlassCheers} /> Cheered</button>);
            }
        }

        return (

            <OverlayTrigger
                placement='top-start'
                trigger={['hover', 'focus']}
                show={this.state.mouseInReactionControl || this.state.reactionControlShow}
                onToggle={() => this.setState({reactionControlShow: !this.state.reactionControlShow})}
                overlay={this.renderReactionControl()}
            >
                {({...triggerHandler}) => (
                    <button className='btn btn-sm news-feed-comment-react-button' {...triggerHandler} onClick={() => NewsFeedState.addCommentReaction(postIndex, index, 'like')}><FontAwesomeIcon icon={faThumbsUp} /> Like</button>
                )}
            </OverlayTrigger>
        );
    }

    render(){
        const {user, content, created} = this.props.data;

        return(
            <div className='news-feed-comment-container d-flex'>
                <div style={{width: '8%'}}>
                    <Link to={`/profile/${user.objectId}`}><Gravatar className='news-feed-post-profile-image' email={user.email} size={30} /></Link>
                </div>
                <div className='news-feed-comment-content' style={{width: '92%'}}>
                    <div className='news-feed-comment-text'>
                        <div className='d-flex'>
                            <div className='mr-auto news-feed-comment-user'>
                                <Link to={`/profile/${user.objectId}`} style={{textDecoration: 'inherit', color: 'inherit'}}>{user.name}</Link>
                            </div>
                            <div className='news-feed-comment-time'>
                                {TimeAgo(created)}
                            </div>
                        </div>
                        {content}
                    </div>
                    <div className='d-flex'>
                        <div>
                            {this.renderReactButton()}
                        </div>
                        <div className='ml-2' style={{fontSize: 12, marginTop: 3}}>
                            {this.renderReactionCounts()}
                        </div>
                    </div>

                </div>
            </div>
        );
    }

}

export default view(NewsFeedComment);