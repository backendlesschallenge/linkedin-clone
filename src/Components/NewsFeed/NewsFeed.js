import React, {Component} from 'react';
import {view} from "@risingstack/react-easy-state";
import NewsFeedState from "../../State/NewsFeedState";
import NewsFeedPost from "./NewsFeedPost";



class NewsFeed extends Component{

    componentDidMount() {
        NewsFeedState.init();
    }

    render(){
        return(
            <div>
                {NewsFeedState.feed.map((feedItem, index) => {
                    return <NewsFeedPost data={feedItem} index={index} key={index} />
                })}
            </div>
        );
    }

}

export default view(NewsFeed);