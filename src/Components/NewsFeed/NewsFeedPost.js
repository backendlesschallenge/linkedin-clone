import React, {Component} from 'react';
import {view} from "@risingstack/react-easy-state";
import Gravatar from 'react-gravatar';
import TimeAgo from '../../Libs/TimeAgo'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faThumbsUp} from "@fortawesome/free-solid-svg-icons/faThumbsUp";
import {faComment} from "@fortawesome/free-solid-svg-icons/faComment";
import {faShare} from "@fortawesome/free-solid-svg-icons/faShare";
import {faThumbsDown} from "@fortawesome/free-solid-svg-icons/faThumbsDown";
import {faGrinSquintTears} from "@fortawesome/free-solid-svg-icons/faGrinSquintTears";
import {faGlassCheers} from "@fortawesome/free-solid-svg-icons/faGlassCheers";
import {faHeart} from "@fortawesome/free-solid-svg-icons/faHeart";
import {OverlayTrigger} from "react-bootstrap";


import './NewsFeedPost.css'
import NewsFeedState from "../../State/NewsFeedState";
import GlobalState from "../../State/GlobalState";
import NewsFeedComment from "./NewsFeedComment";
import {Link} from "react-router-dom";



class NewsFeedPost extends Component{

    state = {
        mouseInReactionControl: false,
        reactionControlShow: false,
        showComments: false,
        commentText: ''
    };

    formatText(text) {
        const formattedText = text.split("\n").map((item, index) => {
            return (
                <span key={index}>
                    {item}
                    <br/>
                </span>
            )
        });
        return formattedText;
    }

    getCurrentReaction() {
        const {reactions} = this.props.data;
        let currentReaction = null;

        reactions.forEach(reaction => {
            if(reaction.user === GlobalState.user.objectId){
                currentReaction = reaction.reaction;
            }
        });

        return currentReaction;
    }

    handleCommentPost(event, index) {
        event.preventDefault();
        NewsFeedState.addComment(index, this.state.commentText);
        this.setState({
            commentText: ''
        })
    }

    renderReactionControlElement(icon, onClick, iconClass) {
        return (
            <button className={`btn btn-light mr-1 news-feed-post-icon-${iconClass}`} onClick={() => {
                onClick();
                this.setState({
                    mouseInReactionControl: false,
                    reactionControlShow: false
                });
            }}><FontAwesomeIcon icon={icon} /></button>
        );
    }

    renderReactionControl(){
        const {index} = this.props;
        return (
            <div
                className='news-feed-post-actions-reaction-container'
                onMouseEnter={() => this.setState({mouseInReactionControl: true})}
                onMouseLeave={() => this.setState({mouseInReactionControl: false})}
            >
                {this.renderReactionControlElement(faThumbsUp, () => NewsFeedState.addReaction(index, 'like'), 'green')}
                {this.renderReactionControlElement(faThumbsDown, () => NewsFeedState.addReaction(index, 'dislike'), 'red')}
                {this.renderReactionControlElement(faGrinSquintTears, () => NewsFeedState.addReaction(index, 'laugh'), 'yellow')}
                {this.renderReactionControlElement(faHeart, () => NewsFeedState.addReaction(index, 'love'), 'pink')}
                {this.renderReactionControlElement(faGlassCheers, () => NewsFeedState.addReaction(index, 'cheers'), 'gray')}
        </div>);
    }

    renderReactionCounts() {
        let likes = 0,
            dislikes = 0,
            laughs = 0,
            loves = 0,
            cheers = 0;

        const {reactions} = this.props.data;

        reactions.forEach(reaction => {
            switch (reaction.reaction.toLowerCase()){
                case 'like':
                    likes++;
                    break;
                case 'dislike':
                    dislikes++;
                    break;
                case 'laugh':
                    laughs++;
                    break;
                case 'love':
                    loves++;
                    break;
                case 'cheers':
                    cheers++;
                    break;
            }
        });

        if(likes || dislikes || laughs || loves || cheers){
            const totalCount = likes + dislikes + laughs + loves + cheers;
            return (
                <div className='news-feed-post-reaction-count-container'>
                    {likes !== 0 && <FontAwesomeIcon icon={faThumbsUp} className='news-feed-post-icon-green' />}
                    {dislikes !== 0 && <FontAwesomeIcon icon={faThumbsDown} className='news-feed-post-icon-red' />}
                    {laughs !== 0 && <FontAwesomeIcon icon={faGrinSquintTears} className='news-feed-post-icon-yellow' />}
                    {loves !== 0 && <FontAwesomeIcon icon={faHeart} className='news-feed-post-icon-pink' />}
                    {cheers !== 0 && <FontAwesomeIcon icon={faGlassCheers} className='news-feed-post-icon-gray' />}
                    <span className='news-feed-post-reaction-count ml-2'>
                        {totalCount}
                    </span>
                </div>
            );
        } else {
            return null;
        }
    }

    renderReactButton() {
        const {index} = this.props;
        const currentReaction = this.getCurrentReaction();
        if(currentReaction){
            switch (currentReaction.toLowerCase()){
                case 'like':
                    return (<button className='btn mr-1 btn-light news-feed-post-icon-green' onClick={() => NewsFeedState.removeReaction(index)}><FontAwesomeIcon icon={faThumbsUp} /> Liked</button>);
                case 'dislike':
                    return (<button className='btn btn-light news-feed-post-icon-red mr-1' onClick={() => NewsFeedState.removeReaction(index)}><FontAwesomeIcon icon={faThumbsDown} /> Disliked</button>);
                case 'laugh':
                    return (<button className='btn btn-light news-feed-post-icon-yellow mr-1' onClick={() => NewsFeedState.removeReaction(index)}><FontAwesomeIcon icon={faGrinSquintTears} /> Laughed</button>);
                case 'love':
                    return (<button className='btn btn-light news-feed-post-icon-pink mr-1' onClick={() => NewsFeedState.removeReaction(index)}><FontAwesomeIcon icon={faHeart} /> Loved</button>);
                case 'cheers':
                    return (<button className='btn btn-light news-feed-post-icon-gray mr-1' onClick={() => NewsFeedState.removeReaction(index)}><FontAwesomeIcon icon={faGlassCheers} /> Cheered</button>);
            }
        }

        return (
            <OverlayTrigger
                placement='top-start'
                trigger={['hover', 'focus']}
                show={this.state.mouseInReactionControl || this.state.reactionControlShow}
                onToggle={() => this.setState({reactionControlShow: !this.state.reactionControlShow})}
                overlay={this.renderReactionControl()}
            >
                {({...triggerHandler}) => (
                    <button className='btn btn-light mr-1' {...triggerHandler} onClick={() => NewsFeedState.addReaction(index, 'like')}><FontAwesomeIcon icon={faThumbsUp} /> Like</button>
                )}
            </OverlayTrigger>
        );
    }

    renderComments(){
        const {user} = this.props.data;
        const {index} = this.props;
        return(
            <form onSubmit={(event) => this.handleCommentPost(event, index)}>
                <div className='news-feed-post-create-comment-container d-flex'>
                    <div style={{width: '8%', paddingTop: '2px'}}>
                        <Gravatar className='news-feed-post-profile-image' email={user.email} size={30} />
                    </div>
                    <div style={{width: '92%'}}>
                        <input
                            className='form-control'
                            placeholder='Add a Comment'
                            style={{borderRadius: 19}}
                            value={this.state.commentText}
                            onChange={event => this.setState({commentText: event.target.value})}
                        />
                        {this.state.commentText && <button className='btn btn-primary fa-pull-right mt-2 btn-sm' style={{borderRadius: 15}}>Post Comment</button>}
                    </div>
                </div>
            </form>
        );
    }

    render() {
        const {content, user, created, comments} = this.props.data;
        const newsFeedIndex = this.props.index;

        return (
            <div className='news-feed-post mb-1 p-2'>
                <div className='d-flex news-feed-post-profile-container-master'>
                    <div className='news-feed-post-profile-image-container'><Link to={`/profile/${user.objectId}`}><Gravatar className='news-feed-post-profile-image' email={user.email} size={60} /></Link></div>
                    <div className='news-feed-post-profile-container'>
                        <div className='news-feed-post-profile-container-name'><Link to={`/profile/${user.objectId}`} style={{textDecoration: 'inherit', color: 'inherit'}}>{user.name}</Link></div>
                        <div className='news-feed-post-profile-container-time'>{TimeAgo(created)}</div>
                    </div>
                </div>
                <div className='news-feed-post-content'>
                    {this.formatText(content)}
                </div>
                {this.renderReactionCounts()}
                <hr />
                <div className='news-feed-post-actions'>
                    {this.renderReactButton()}
                    <button className='btn btn-light mr-1' onClick={() => this.setState({showComments: !this.state.showComments})}><FontAwesomeIcon icon={faComment} /> Comment</button>
                    <button className='btn btn-light'><FontAwesomeIcon icon={faShare} /> Share</button>
                </div>
                {this.state.showComments && this.renderComments()}
                {this.state.showComments && comments && comments.map((comment, index) => {
                    return <NewsFeedComment data={comment} postIndex={newsFeedIndex} index={index} key={index} />
                })}
            </div>
        );
    }

}

export default view(NewsFeedPost);