import React from 'react';
import {view} from "@risingstack/react-easy-state";
import './HomeHero.css'

import heroImage from './linked-in-home-hero.svg'

function HomeHero(){
    return (
        <div className='home-hero'>
            <img alt='Home Hero Image' className='home-hero-image' src={heroImage} />
            <div className='container'>
                <div className='row'>
                    <div className='col-7'>
                        <h2 className='home-hero-title'>Welcome to your professional community</h2>
                        <button className="btn btn-outline-info home-hero-button">Search for a job</button>
                        <button className="btn btn-outline-info home-hero-button">Find a person you know</button>
                        <button className="btn btn-outline-info home-hero-button">Learn a new skill</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default view(HomeHero);