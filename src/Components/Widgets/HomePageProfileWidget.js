import React, {Component} from "react";
import {view} from "@risingstack/react-easy-state";

import './HomePageProfileWidget.css'
import GlobalState from "../../State/GlobalState";
import {Link} from "react-router-dom";
import Gravatar from 'react-gravatar';

class HomePageProfileWidget extends Component{

    render(){
        return(
            <div className='home-page-profile-widget'>
                <div className='home-page-profile-widget-image-container'>
                    <Gravatar
                        email={GlobalState.user.email}
                        className='home-page-profile-widget-image'
                        size={70}
                    />
                </div>
                <div className='home-page-profile-widget-profile text-center'>
                    <Link to={`/profile/${GlobalState.user.objectId}`}>{GlobalState.user.name}</Link><br />
                    Job Title
                </div>
                <div className='home-page-profile-widget-overview'>
                    <div className='home-page-profile-widget-overview-item d-flex ml-2 mr-2'>
                        <div className='mr-auto'>
                            Who viewed your profile
                        </div>
                        <div>
                            19
                        </div>
                    </div>
                    <div className='home-page-profile-widget-overview-item d-flex ml-2 mr-2'>
                        <div className='mr-auto'>
                            Connections
                        </div>
                        <div>
                            518
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default view(HomePageProfileWidget);