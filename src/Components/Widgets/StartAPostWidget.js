import React, {Component} from "react";
import {view} from "@risingstack/react-easy-state";

import './StartAPostWidget.css'
import {Link} from "react-router-dom";
import ModalState from "../../State/ModalState";

class StartAPostWidget extends Component{

    state = {
        showCreatePostModal: false
    };

    render(){
        return(
            <div className='start-a-post-widget'>
                <div className='m-3'>
                    <div onClick={() => {
                        ModalState.showCreateAPost = true
                    }} type='text' className='form-control' placeholder='Start a post'>Start a post</div>
                </div>
                <div className='d-flex justify-content-between m-3'>
                    <button className='btn btn-sm btn-light'>Photo</button>
                    <button className='btn btn-sm btn-light'>Event</button>
                    <Link to='/articles' className='btn btn-sm btn-light'>Write article</Link>
                </div>
            </div>
        );
    }

}

export default view(StartAPostWidget);