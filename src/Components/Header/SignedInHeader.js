import React, {Component} from 'react';
import {view} from "@risingstack/react-easy-state";
import {Link} from "react-router-dom";

class SignedInHeader extends Component{

    render(){
        return(
            <nav className='navbar navbar-expand-lg navbar-light bg-light'>
                <div className='container'>
                    <Link to='/' className='navbar-brand'>VineScale</Link>
                    <button className='navbar-toggler' type='button'>
                        <span className="navbar-toggler-icon"/>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <form className="form-inline my-2 my-lg-0 mr-auto">
                            <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                            <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </form>
                        <ul className="navbar-nav">
                            <li className="nav-item active">
                                <Link className="nav-link" to="/">Home</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/my-network">My Network</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/jobs">Jobs</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/messages">Messaging</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/notifications">Notifications</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/me">Me</Link>
                            </li>
                        </ul>

                    </div>
                </div>

            </nav>
        );
    }

}

export default view(SignedInHeader);