import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {view} from "@risingstack/react-easy-state";
import './SignedOutHeader.css'

class SignedOutHeader extends Component{

    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-6 header-logo'>
                        <Link to='/' style={{textDecoration: 'none'}}>VineScale</Link>
                    </div>
                    <div className='col-6 text-right header-right'>
                        <Link to='/sign-up' className='mr-2 btn btn-outline-info header-join-cta'>Join now</Link>
                        <Link to='/sign-in' className='btn btn-outline-info header-signin-cta'>Sign in</Link>
                    </div>
                </div>
            </div>
        );
    }

}


export default view(SignedOutHeader);