import React, {Component} from 'react';
import {view} from "@risingstack/react-easy-state";
import Modal from "react-bootstrap/cjs/Modal";
import ModalState from "../../State/ModalState";
import Backendless from 'backendless';
import NewsFeedState from "../../State/NewsFeedState";
import GlobalState from "../../State/GlobalState";

class CreateAPostModal extends Component{

    state = {
        content: ''
    }

    handlePostCreate() {
        NewsFeedState.addPost(this.state.content);
        ModalState.showCreateAPost = false
        this.setState({
            content: '',
            reactions: {}
        })
    }

    render() {
        return(
            <Modal show={ModalState.showCreateAPost} onHide={() => {
                ModalState.showCreateAPost = false
            }}>
                <Modal.Header closeButton>
                    <Modal.Title>Create a Post</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <textarea value={this.state.content} onChange={(event) => this.setState({content: event.target.value})} className='form-control' placeholder='What do you want to talk about?' rows={8} style={{border: 'none'}} />
                </Modal.Body>
                <Modal.Footer>
                    <button className="btn btn-secondary" onClick={() => {
                        ModalState.showCreateAPost = false
                    }}>
                        Cancel
                    </button>
                    <button className="btn btn-primary" onClick={() => this.handlePostCreate()}>
                        Post
                    </button>
                </Modal.Footer>
            </Modal>
        )
    }
}

export default view(CreateAPostModal);