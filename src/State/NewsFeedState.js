import {store} from "@risingstack/react-easy-state";
import GlobalState from "./GlobalState";
import Backendless from 'backendless'
import ModalState from "./ModalState";

const NewsFeedState = store({
    isInit: false,
    isLoading: false,
    feed: [],
    postRelations: ['user', 'comments', 'comments.user'],
    init: () => {
        if(NewsFeedState.isInit) return;
        NewsFeedState.isInit = true;
        NewsFeedState.refresh();
    },
    refresh: () => {
        if(!NewsFeedState.isInit) return;
        let inValues = '';

        NewsFeedState.getNewsFeedUsers().forEach(userId => {
            inValues = (inValues === '' ? '' : `${inValues},`);
            inValues = `${inValues} '${userId}'`;
        });

        let whereClause = `ownerId IN(${inValues})`

        const queryBuilder = Backendless.DataQueryBuilder.create().setWhereClause(whereClause).setRelated(NewsFeedState.postRelations).setPageSize(100).setSortBy('created DESC');

        Backendless.Data.of('posts').find(queryBuilder).then(result => {
            NewsFeedState.feed = result;
            console.log('news feed', result);
        }).catch(error => {
            console.log(error.message)
        });
    },
    refreshPost: async (index) => {
        const post = NewsFeedState.feed[index];
        NewsFeedState.feed[index] = await Backendless.Data.of('posts').findById(post.objectId, {
            relations: NewsFeedState.postRelations
        })
    },
    addPost: async(content) => {
        const post = await Backendless.Data.of('posts').save({
            content,
            reactions: []
        });

        const relation = await Backendless.Data.of('posts').setRelation({objectId: post.objectId}, 'user', [{objectId: GlobalState.user.objectId}]);

        NewsFeedState.refresh();
    },
    refreshComments: (index) => {

    },
    addComment: async (index, content) => {
        const post = NewsFeedState.feed[index];

        const comment = await Backendless.Data.of('post_comments').save({
            content,
            reactions: []
        });

        await Backendless.Data.of('post_comments').setRelation({objectId: comment.objectId}, 'user', [{objectId: GlobalState.user.objectId}]);
        await Backendless.Data.of('posts').addRelation({objectId: post.objectId}, 'comments', [{objectId: comment.objectId}]);
        await NewsFeedState.refreshPost(index);
    },
    addReaction: async (index, reaction) => {
        const post = NewsFeedState.feed[index];

        const newReaction = {
            user: GlobalState.user.objectId,
            reaction: reaction
        };

        const updateBuilder = Backendless.JSONUpdateBuilder.ARRAY_APPEND().addArgument(`$`, newReaction);

        const result = await Backendless.Data.of('posts').save({
            objectId: post.objectId,
            reactions: updateBuilder
        });

        NewsFeedState.feed[index].reactions = result.reactions;
    },
    removeReaction: async(index) => {
        let removeIndex = -1;
        NewsFeedState.feed[index].reactions.forEach((reaction, reactionIndex) => {
            if(reaction.user === GlobalState.user.objectId){
                removeIndex = reactionIndex;
            }
        });
        console.log(removeIndex);

        if(removeIndex !== -1){
            const updateBuilder = Backendless.JSONUpdateBuilder.REMOVE().addArgument(`$[${removeIndex}]`);

            const result = await Backendless.Data.of('posts').save({
                objectId: NewsFeedState.feed[index].objectId,
                reactions: updateBuilder
            });
            NewsFeedState.feed[index].reactions = result.reactions;
        }
    },
    addCommentReaction: async(postIndex, commentIndex, reaction) => {
        const comment = NewsFeedState.feed[postIndex].comments[commentIndex];

        const newReaction = {
            user: GlobalState.user.objectId,
            reaction: reaction
        };

        const updateBuilder = Backendless.JSONUpdateBuilder.ARRAY_APPEND().addArgument(`$`, newReaction);

        const result = await Backendless.Data.of('post_comments').save({
            objectId: comment.objectId,
            reactions: updateBuilder
        });

        NewsFeedState.feed[postIndex].comments[commentIndex].reactions = result.reactions;
    },
    removeCommentReaction: async(postIndex, commentIndex) => {
        let removeIndex = -1;
        NewsFeedState.feed[postIndex].comments[commentIndex].reactions.forEach((reaction, reactionIndex) => {
            if(reaction.user === GlobalState.user.objectId){
                removeIndex = reactionIndex;
            }
        });

        if(removeIndex !== -1) {
            const updateBuilder = Backendless.JSONUpdateBuilder.REMOVE().addArgument(`$[${removeIndex}]`);

            const result = await Backendless.Data.of('post_comments').save({
                objectId: NewsFeedState.feed[postIndex].comments[commentIndex].objectId,
                reactions: updateBuilder
            });
            NewsFeedState.feed[postIndex].comments[commentIndex].reactions = result.reactions;
        }
    },
    getNewsFeedUsers: () => {
        return [
            GlobalState.user.objectId
        ]
    }
});

export default NewsFeedState;