import {store} from "@risingstack/react-easy-state";

const ModalState = store({
    showCreateAPost: false
});

export default ModalState;