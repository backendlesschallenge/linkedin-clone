import {store} from "@risingstack/react-easy-state";
import Backendless from 'backendless';

const GlobalState = store({
    hasInit: false,
    isLoading: false,
    isLoggedIn: false,
    user: {},
    init: () => {
        if(GlobalState.hasInit) return;

        Backendless.initApp('9180B113-8A09-B113-FFBB-A6DCF1E45900', '4B71730F-4FCA-4114-A0C2-D318E8698990');

        GlobalState.hasInit = true;
        GlobalState.isLoading = true;

        Backendless.UserService.getCurrentUser().then(result => {
            if(result){
                GlobalState.isLoggedIn = true;
                GlobalState.user = result;
                console.log('user is signed in', result);
            }
            GlobalState.isLoading = false;
        });
    }
});

export default GlobalState;