import TimeAgoLib from 'javascript-time-ago';
import en from 'javascript-time-ago/locale/en'
TimeAgoLib.addDefaultLocale(en)

function TimeAgo(time){
    const timeAgo = new TimeAgoLib('en');
    return timeAgo.format(time);
}

export default TimeAgo;