import React, {Component} from 'react';
import {view} from "@risingstack/react-easy-state";
import {Link} from "react-router-dom";
import Backendless from "backendless";

class SignUp extends Component{

    state = {
        email: '',
        password: '',
        errorMessage: '',
        isProcessing: false
    };

    handleLogin() {
        if(this.state.isProcessing){
            return;
        }
        this.setState({
            isProcessing: true
        });

        Backendless.UserService.login(this.state.email, this.state.password, true).then(() => {
            window.location = '/';
        }).catch(error => {
            this.setState({
                errorMessage: error.message,
                isProcessing: false
            });
        });
    }


    render(){
        return(
            <div className='sign-up-page'>
                <div className='container sign-up-page-container'>
                    <div>
                        <Link to='/' className='sign-up-logo' style={{textDecoration: 'none'}}>VineScale</Link>
                    </div>
                    <div>
                        <h2 className='sign-up-subtitle'>Make the most of your professional life</h2>
                    </div>
                    <div className='row'>
                        <div className='col-6 offset-3'>
                            <div className='sign-up-page-form-container'>
                                <div style={{color: 'red'}}>{this.state.errorMessage}</div>
                                <div>
                                    <label htmlFor='sign-up-email'>Email</label>
                                    <input value={this.state.email} onChange={(event) => this.setState({email: event.target.value})} id='sign-up-email' type='text' className='form-control' />
                                </div>
                                <div className='sign-up-password-field-container'>
                                    <label htmlFor='sign-up-password'>Password</label>
                                    <input value={this.state.password} onChange={(event) => this.setState({password: event.target.value})} id='sign-up-password' type='password' className='form-control' />
                                </div>
                                <div className='sign-up-password-field-container'>
                                    <button onClick={() => this.handleLogin()} className='btn btn-lg btn-info btn-block sign-up-page-button'>Sign in</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default view(SignUp);