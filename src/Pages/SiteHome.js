import React, {Component} from 'react';
import {view} from "@risingstack/react-easy-state";
import SignedOutHeader from "../Components/Header/SignedOutHeader";
import HomeHero from "../Components/Hero/HomeHero";
import HomeSuggestedSearches from "../Components/Section/HomeSuggestedSearches";
import HomePostJob from "../Components/Section/HomePostJob";

class SiteHome extends Component{

    render(){
        return (
            <div>
                <SignedOutHeader />
                <HomeHero />
                <HomeSuggestedSearches />
                <HomePostJob />
            </div>
        );
    }

}


export default view(SiteHome);