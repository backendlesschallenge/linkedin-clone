import React, {Component} from 'react';
import {view} from "@risingstack/react-easy-state";
import HomePageProfileWidget from "../Components/Widgets/HomePageProfileWidget";
import StartAPostWidget from "../Components/Widgets/StartAPostWidget";
import NewsFeed from "../Components/NewsFeed/NewsFeed";

class UserHome extends Component{

    render(){
        return(
            <div className='row mt-3'>
                <div className='col-3'>
                    <HomePageProfileWidget />
                </div>
                <div className='col-6'>
                    <StartAPostWidget />
                    <hr />
                    <NewsFeed />
                </div>
                <div className='col-3'>
                    Sidebar
                </div>
            </div>
        )
    }

}

export default view(UserHome);